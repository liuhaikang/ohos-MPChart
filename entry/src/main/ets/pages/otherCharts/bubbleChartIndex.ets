/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import title from '../title/index';
import { BubbleEntry } from '@ohos/mpchart'
import { BubbleData } from '@ohos/mpchart'
import { BubbleDataSet } from '@ohos/mpchart'
import { IBubbleDataSet } from '@ohos/mpchart'
import { MPPointF } from '@ohos/mpchart'
import { JArrayList } from '@ohos/mpchart'
import { MyRect } from '@ohos/mpchart'
import { TextPaint, ImagePaint } from '@ohos/mpchart'
import { XAxis, XAxisPosition } from '@ohos/mpchart'
import { YAxis, YAxisLabelPosition, AxisDependency } from '@ohos/mpchart'
import { Utils } from '@ohos/mpchart'
import { BubbleChart } from '@ohos/mpchart'
import { BubbleChartMode } from '@ohos/mpchart'
import { SeekBar } from '@ohos/mpchart'

@Entry
@Component
struct bubbleChartIndex {
  //标题栏菜单文本
  private menuItemArr: Array<string> = [/*'View on GitHub', */'Toggle Values', 'Toggle Icons',
    'Toggle Highlight', /*'Toggle PinchZoom','Toggle Auto Scale',*/'Animate X', 'Animate Y', 'Animate XY'/*,'Save to Gallery'*/];
  //标题栏标题
  private title: string = 'BubbleChartActivity'
  @State @Watch("menuCallback") model: title.Model = new title.Model()

  //标题栏菜单回调
  menuCallback() {
    if (this.model == null || this.model == undefined) {
      return
    }
    let index: number = this.model.getIndex()
    if (index == undefined || index == -1) {
      return
    }
    switch (this.menuItemArr[index]) {
      case 'View on GitHub':
      //TODO View on GitHub
        break;
      case 'Toggle Values':
        this.setShowValues()
        break;
      case 'Toggle Icons':
        this.setShowIcons();
        break;
      case 'Toggle Highlight':
        this.setShowHighlight()
        break;
      case 'Toggle PinchZoom':
        break;
      case 'Toggle Auto Scale':
        break;
      case 'Animate X':
        this.bubbleChartMode.animate('X', 2000, 20);
        break;
      case 'Animate Y':
        this.bubbleChartMode.animate('Y', 2000, 20)
        break;
      case 'Animate XY':
        this.bubbleChartMode.animate('XY', 2000, 20)
        break;

    }
    this.model.setIndex(-1)
  }

  topAxis: XAxis = new XAxis(); //顶部X轴
  bottomAxis: XAxis = new XAxis(); //底部X轴
  mWidth: number = 300; //表的宽度
  mHeight: number = 300; //表的高度
  minOffset: number = 15; //X轴线偏移量
  leftAxis: YAxis = new YAxis();
  rightAxis: YAxis = new YAxis();
  data: BubbleData = new BubbleData();
  isDrawValuesEnable: boolean = false;
  @State
  bubbleChartMode: BubbleChartMode = new BubbleChartMode();
  @State
  @Watch('xSeekBarWatch')
  xSeekBar: SeekBar.Model = new SeekBar.Model()
  @State
  @Watch('ySeekBarWatch')
  ySeekBar: SeekBar.Model = new SeekBar.Model()

  build() {
    Column() {
      title({ model: this.model })
      Stack({ alignContent: Alignment.TopStart }) {
        BubbleChart({
          bubbleChartMode: this.bubbleChartMode
        })
      }.height(this.bubbleChartMode.mHeight)

      Button("切换数据").onClick(() => {
        this.changeData();
      }).margin(30)
      //      Column(){
      //        SeekBar({ model: this.xSeekBar })
      //        SeekBar({ model: this.ySeekBar })
      //      }.width('80%')
    }
  }

  changeData() {
    this.data = this.initBubbleData(5, 50);
    this.bubbleChartMode.data = this.data;
    this.bubbleChartMode.invalidate();
  }

  aboutToAppear() {
    //    this.xSeekBar.setValue(5)
    //      .setMax(25)
    //      .setMin(0)
    //
    //    this.ySeekBar.setValue(50)
    //      .setMax(200)
    //      .setMin(0)
    this.model.menuItemArr = this.menuItemArr
    this.model.title = this.title
    this.data = this.initBubbleData(5, 50)


    this.topAxis.setLabelCount(6, false);
    this.topAxis.setPosition(XAxisPosition.TOP);
    this.topAxis.setAxisMinimum(0);
    this.topAxis.setAxisMaximum(this.data.getXMax());
    this.topAxis.enableGridDashedLine(10, 10, 0)
    this.topAxis.setDrawAxisLine(true);
    this.topAxis.setDrawLabels(false)

    this.bottomAxis.setLabelCount(6, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setAxisMaximum(this.data.getXMax());
    this.bottomAxis.setDrawAxisLine(true);
    this.bottomAxis.setDrawLabels(true)

    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(5, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(0);
    this.leftAxis.setAxisMaximum(this.data.getYMax());
    this.leftAxis.enableGridDashedLine(10, 10, 0)

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setLabelCount(7, false);
    this.rightAxis.setSpaceTop(15);
    this.rightAxis.setAxisMinimum(0); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(this.data.getYMax());
    this.rightAxis.setDrawAxisLine(false);
    this.rightAxis.setDrawLabels(false)

    let textPaint: TextPaint = new TextPaint();
    textPaint.setTextSize(this.leftAxis.getTextSize());
    let leftTextWidth = Utils.calcTextWidth(textPaint, this.getFormattedValue(this.leftAxis.getAxisMaximum()));
    let left = this.leftAxis.getSpaceTop() + leftTextWidth;
    let top = this.minOffset;
    let right = this.mWidth - this.minOffset - leftTextWidth;
    let bottom = this.mHeight - this.minOffset;
    this.data.mDisplayRect = new MyRect(left, top, right, bottom);

    this.bubbleChartMode.setYExtraOffset(this.model.height)
    this.bubbleChartMode.topAxis = this.topAxis
    this.bubbleChartMode.bottomAxis = this.bottomAxis
    this.bubbleChartMode.mWidth = this.mWidth
    this.bubbleChartMode.mHeight = this.mHeight
    this.bubbleChartMode.minOffset = this.minOffset
    this.bubbleChartMode.leftAxis = this.leftAxis
    this.bubbleChartMode.rightAxis = this.rightAxis
    this.bubbleChartMode.data = this.data
    this.bubbleChartMode.init()

    this.setXAxisMode();

    this.bubbleChartMode.leftAxisModel.setWidth(this.bubbleChartMode.mWidth)
    this.bubbleChartMode.leftAxisModel.setHeight(this.bubbleChartMode.mHeight)
    this.bubbleChartMode.leftAxisModel.setMinOffset(this.bubbleChartMode.minOffset)
    this.bubbleChartMode.leftAxisModel.setYAxis(this.bubbleChartMode.leftAxis)

    this.bubbleChartMode.rightAxisModel.setWidth(this.bubbleChartMode.mWidth)
    this.bubbleChartMode.rightAxisModel.setHeight(this.bubbleChartMode.mHeight)
    this.bubbleChartMode.rightAxisModel.setMinOffset(this.bubbleChartMode.minOffset)
    this.bubbleChartMode.rightAxisModel.setYAxis(this.bubbleChartMode.rightAxis)
    /* leftAxisModel:YAxisModel = new YAxisModel();
    rightAxisModel:YAxisModel = new YAxisModel();*/
  }

  public setXAxisMode() {
    this.bubbleChartMode.xAixsMode.topAxis = this.bubbleChartMode.topAxis
    this.bubbleChartMode.xAixsMode.bottomAxis = this.bubbleChartMode.bottomAxis
    this.bubbleChartMode.xAixsMode.mWidth = this.bubbleChartMode.mWidth
    this.bubbleChartMode.xAixsMode.mHeight = this.bubbleChartMode.mHeight
    this.bubbleChartMode.xAixsMode.minOffset = this.bubbleChartMode.minOffset
    this.bubbleChartMode.xAixsMode.yLeftLongestLabel = this.getFormattedValue(this.bubbleChartMode.leftAxis.getAxisMaximum())
    this.bubbleChartMode.xAixsMode.yRightLongestLabel = this.getFormattedValue(this.bubbleChartMode.rightAxis.getAxisMaximum())
  }

  /**
   * 初始化数据
   * @param count  曲线图点的个数
   * @param range  y轴范围
   */
  private initBubbleData(count: number, range: number): BubbleData {

    let values1 = new JArrayList<BubbleEntry>();
    let values2 = new JArrayList<BubbleEntry>();
    let values3 = new JArrayList<BubbleEntry>();
    let imgePaint: ImagePaint = new ImagePaint();
    imgePaint.setIcon($r('app.media.star'))
    imgePaint.setWidth(px2vp(32))
    imgePaint.setHeight(px2vp(32));
    //
    for (let i = 0; i < count; i++) {
      values1.add(new BubbleEntry(i, Math.random() * range, Math.random() * range, imgePaint));
      values2.add(new BubbleEntry(i, Math.random() * range, Math.random() * range, imgePaint));
      values3.add(new BubbleEntry(i, Math.random() * range, Math.random() * range));
    }
    //        values1.add(new BubbleEntry(1, 10, Math.random() * range,imgePaint));
    //        values1.add(new BubbleEntry(1.6, 10, Math.random() * range,imgePaint));
    //        values1.add(new BubbleEntry(3.5, 2, Math.random() * range,imgePaint));
    //        values1.add(new BubbleEntry( 3.8,40,Math.random() * range,imgePaint));
    //        values1.add(new BubbleEntry(4, 8, Math.random() * range,imgePaint));
    //
    //    values2.add(new BubbleEntry(0.8, 6, Math.random() * range,imgePaint));
    //    values2.add(new BubbleEntry(1.8, 20, Math.random() * range,imgePaint));
    //    values2.add(new BubbleEntry(2.2, 39, Math.random() * range,imgePaint));
    //    values2.add(new BubbleEntry(3.5, 27, Math.random() * range,imgePaint));
    //    values2.add(new BubbleEntry(4.5, 46, Math.random() * 0,imgePaint));
    //
    //    values3.add(new BubbleEntry(0.6, 25, Math.random() * range,imgePaint));
    //    values3.add(new BubbleEntry(1.8, 20, Math.random() * range,imgePaint));
    //    values3.add(new BubbleEntry(2.2, 15, Math.random() * range,imgePaint));
    //    values3.add(new BubbleEntry(3.5, 8, Math.random() * range,imgePaint));
    //    values3.add(new BubbleEntry(2.5, 29, Math.random() * range,imgePaint));

    let set1: BubbleDataSet = new BubbleDataSet(values1, "DS 1");
    set1.setDrawIcons(false);
    set1.setColorByColor(0x88c12552);
    set1.setIconsOffset(new MPPointF(0, px2vp(0)));
    set1.setDrawValues(this.isDrawValuesEnable);


    let set2: BubbleDataSet = new BubbleDataSet(values2, "DS 2");
    set2.setDrawIcons(false);
    set2.setIconsOffset(new MPPointF(0, px2vp(0)));
    set2.setColorByColor(0x88ff6600);
    set2.setDrawValues(this.isDrawValuesEnable);

    let set3: BubbleDataSet = new BubbleDataSet(values3, "DS 3");
    set3.setDrawIcons(false);
    set3.setIconsOffset(new MPPointF(0, 0));
    set3.setColorByColor(0x88f5c700);
    set3.setDrawValues(this.isDrawValuesEnable);


    let dataSets = new JArrayList<IBubbleDataSet>();
    dataSets.add(set1);
    dataSets.add(set2);
    dataSets.add(set3);
    let dataResult: BubbleData = new BubbleData(dataSets);
    dataResult.setDrawValues(this.isDrawValuesEnable);
    dataResult.setValueTextSize(8);
    dataResult.setValueTextColor(Color.White);
    dataResult.setHighlightCircleWidth(1.5);
    dataResult.setHighlightEnabled(true);
    return dataResult;
  }

  public getFormattedValue(value: number): string {
    return value.toFixed(0)
  }

  public setShowValues() {
    this.isDrawValuesEnable = (!this.isDrawValuesEnable)
    this.bubbleChartMode.data.setDrawValues(this.isDrawValuesEnable);
    this.bubbleChartMode.drawChart();
  }

  public setShowIcons() {
    for (let dataSet of this.bubbleChartMode.data.getDataSets().dataSource) {
      dataSet.setDrawIcons(!dataSet.isDrawIconsEnabled());
    }
    this.bubbleChartMode.data.setDrawValues(this.isDrawValuesEnable);
    this.bubbleChartMode.drawChart();
  }

  public setShowHighlight() {
    this.bubbleChartMode.data.setHighlightEnabled(!this.bubbleChartMode.data.isHighlightEnabled());
    this.bubbleChartMode.drawHighLight();
  }

  private xSeekBarWatch() {
    let xValue = this.xSeekBar.getValue().toFixed(0);
    let yValue = this.ySeekBar.getValue().toFixed(0);
    console.log("xSeekBarWatch:" + xValue)
  }

  private ySeekBarWatch() {
    let yValue = this.ySeekBar.getValue().toFixed(0);
    console.log("ySeekBarWatch:" + yValue)
  }
}









