/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ImagePaint } from '@ohos/mpchart';
import { Fill } from '@ohos/mpchart';
import { XAxis, XAxisPosition } from '@ohos/mpchart'
import { YAxis, AxisDependency, YAxisLabelPosition } from '@ohos/mpchart'
import { BarEntry } from '@ohos/mpchart';
import { JArrayList } from '@ohos/mpchart';
import { BarDataSet } from '@ohos/mpchart';
import { BarData } from '@ohos/mpchart';
import { BarChart, BarChartModel } from '@ohos/mpchart'
import { IBarDataSet } from '@ohos/mpchart'
import title from '../title/index';


@Entry
@Component
struct Index_bar_charts {
  @State model: BarChartModel = new BarChartModel();
  mWidth: number = 350; //表的宽度
  mHeight: number = 500; //表的高度
  minOffset: number = 15; //X轴线偏移量
  leftAxis: YAxis | null = null;
  rightAxis: YAxis | null = null;
  bottomAxis: XAxis = new XAxis();
  //标题栏菜单文本
  private menuItemArr: Array<string> = ['Toggle Bar Borders', 'Animate X', 'Animate Y', 'Animate XY'];
  //标题栏标题
  private title: string = 'BarChart'
  @State @Watch("menuCallback") titleModel: title.Model = new title.Model()

  //标题栏菜单回调
  menuCallback() {
    if (this.titleModel == null || this.titleModel == undefined) {
      return
    }
    let index: number = this.titleModel.getIndex()
    if (index == undefined || index == -1) {
      return
    }
    switch (this.menuItemArr[index]) {
      case 'Toggle Bar Borders':
        for (let i = 0;i < this.model.getBarData().getDataSets().length(); i++) {
          let barDataSet = this.model.getBarData().getDataSets().get(i) as BarDataSet;
          barDataSet.setBarBorderWidth(barDataSet.getBarBorderWidth() == 1 ? 0 : 1)
        }
        this.model.invalidate()
        break;
      case 'Animate X':
        this.model.animateX(2000)
        break;
      case 'Animate Y':
        this.model.animateY(2000)
        break;
      case 'Animate XY':
        this.model.animateXY(2000, 2000)
        break;

    }
    this.titleModel.setIndex(-1)
  }

  public aboutToAppear() {
    this.titleModel.menuItemArr = this.menuItemArr
    this.titleModel.title = this.title
    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(11, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(0);
    this.leftAxis.setAxisMaximum(100);
    this.leftAxis.enableGridDashedLine(10, 10, 0)

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setLabelCount(7, false);
    this.rightAxis.setSpaceTop(11);
    this.rightAxis.setAxisMinimum(0); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(100);

    this.bottomAxis.setLabelCount(6, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setAxisMaximum(12);

    this.setData(this.bottomAxis.getAxisMaximum(), this.leftAxis.getAxisMaximum())

    this.model.setWidth(this.mWidth);
    this.model.setHeight(this.mHeight);
    this.model.init();
    this.model.setDrawBarShadow(false);
    this.model.setDrawValueAboveBar(true);
    this.model.getDescription().setEnabled(false);
    this.model.setMaxVisibleValueCount(60);
    this.model.setLeftYAxis(this.leftAxis);
    this.model.setRightYAxis(this.rightAxis);
    this.model.setXAxis(this.bottomAxis)
    this.model.mRenderer.initBuffers();
    this.model.prepareMatrixValuePx();

  }

  private changeData() {
    if (this.leftAxis) {
      this.setData(this.bottomAxis.getAxisMaximum(), this.leftAxis.getAxisMaximum())
      this.model.invalidate();
    }
  }

  build() {
    Column() {
      title({ model: this.titleModel })
      Stack({ alignContent: Alignment.TopStart }) {
        BarChart({ model: this.model })
      }

      Button("切换数据").onClick(() => {
        this.changeData();
      }).margin(30).position({x:'0%', y:'80%'})
    }.alignItems(HorizontalAlign.Start)
  }

  private setData(count: number, range: number) {

    let start: number = 0;

    let values: JArrayList<BarEntry> = new JArrayList<BarEntry>();

    for (let i = start; i < count; i++) {
      let val: number = (Math.random() * range);
      if (Math.random() * 100 < 25) {
        let paint: ImagePaint = new ImagePaint();
        paint.setIcon("app.media.star");
        values.add(new BarEntry(i, val, paint));
      } else {
        values.add(new BarEntry(i, val));
      }
    }

    let set1: BarDataSet;

    if (this.model.getBarData() != null &&
      this.model.getBarData().getDataSetCount() > 0) {
      set1 = this.model.getBarData().getDataSetByIndex(0) as BarDataSet;
      set1.setValues(values);
      this.model.getBarData().notifyDataChanged();
      this.model.notifyDataSetChanged();

    } else {
      set1 = new BarDataSet(values, "The year 2017");

      set1.setDrawIcons(false);

      let startColor1: number = 0xffffbb33;
      let startColor2 = 0xff33b5e5;
      let startColor3 = 0xffffbb33;
      let startColor4 = 0xff99cc00;
      let startColor5 = 0xffff4444;
      let endColor1 = 0xff0099cc;
      let endColor2 = 0xffaa66cc;
      let endColor3 = 0xff669900;
      let endColor4 = 0xffcc0000;
      let endColor5 = 0xffff8800;

      let gradientFills: JArrayList<Fill> = new JArrayList<Fill>();
      gradientFills.add(new Fill(undefined, startColor1, endColor1));
      gradientFills.add(new Fill(undefined, startColor2, endColor2));
      gradientFills.add(new Fill(undefined, startColor3, endColor3));
      gradientFills.add(new Fill(undefined, startColor4, endColor4));
      gradientFills.add(new Fill(undefined, startColor5, endColor5));

      set1.setFills(gradientFills);

      let dataSets: JArrayList<IBarDataSet> = new JArrayList<IBarDataSet>();
      dataSets.add(set1);

      let data: BarData = new BarData(dataSets);
      data.setValueTextSize(10);

      this.model.setData(data);
    }
  }
}