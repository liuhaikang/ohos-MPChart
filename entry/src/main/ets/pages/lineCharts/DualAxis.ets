/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {ColorTemplate} from '@ohos/mpchart'
import {Legend,LegendForm} from '@ohos/mpchart'
import { LineChart,LineChartModel } from '@ohos/mpchart'
import {XAxis, XAxisPosition} from '@ohos/mpchart'
import {YAxis,AxisDependency, YAxisLabelPosition} from '@ohos/mpchart'
import { LineData } from '@ohos/mpchart'
import {LineDataSet,ColorStop,Mode} from '@ohos/mpchart'
import { EntryOhos } from '@ohos/mpchart'
import {JArrayList} from '@ohos/mpchart'
import { ILineDataSet } from '@ohos/mpchart'
import {LimitLine,LimitLabelPosition} from '@ohos/mpchart'
import title from '../title/index';

@Entry
@Component
struct DualAxis {
  //标题栏菜单文本
  private menuItemArr: Array<string> = ['Animate X','Animate Y','Animate XY'];

  //标题栏标题
  private title: string = 'LineChartActivity 2'
  @State @Watch("menuCallback") model: title.Model = new title.Model()

  topAxis: XAxis = new XAxis(); //顶部X轴
  bottomAxis: XAxis = new XAxis(); //底部X轴
  mWidth: number = 350; //表的宽度
  mHeight: number = 300; //表的高度
  minOffset: number = 15; //X轴线偏移量
  leftAxis: YAxis | null = null;
  rightAxis: YAxis | null = null;
  lineData: LineData | null = null;
  @State
  lineChartModel: LineChartModel = new LineChartModel();
  titleSelcetString:string='X'

  //标题栏菜单回调
  menuCallback(){
    if (this.model == null || this.model == undefined) {
      return
    }
    let index: number = this.model.getIndex()
    if(index==undefined||index==-1){
      return
    }
    switch (this.menuItemArr[index]) {
      case 'View on GitHub':
      //TODO View on GitHub
        break;
      case 'Toggle Values':
        break;
      case 'Toggle Icons':
        break;
      case 'Toggle Filled':
        break;
      case 'Toggle Highlight':
        break;
      case 'Toggle Highlight Circle':
        break;
      case 'Toggle Rotation':
        break;
      case 'Toggle X-Values':
        break;
      case 'Spin Animation':
        break;
      case 'Animate X':
        this.titleSelcetString='X'
        this.animate()
        break;
      case 'Animate Y':
        this.titleSelcetString='Y'
        this.animate()
        break;
      case 'Animate XY':
        this.titleSelcetString='XY'
        this.animate()
        break;

    }
    this.model.setIndex(-1)
  }

  public animate() {
    if (this.titleSelcetString == 'X') {
      this.lineChartModel.pathViewModel.animateX(60);
    } else if (this.titleSelcetString == 'Y') {
      this.lineChartModel.pathViewModel.animateY(60);
    } else if (this.titleSelcetString == 'XY') {
      this.lineChartModel.pathViewModel.animateXY(60);
    }

  }

  aboutToAppear() {
    this.model.menuItemArr = this.menuItemArr
    this.model.title = this.title

    this.lineData = this.initCurveData(20, 30);

    this.topAxis.setPosition(XAxisPosition.TOP);
    this.topAxis.setAxisMinimum(this.lineData.getXMin());
    this.topAxis.setAxisMaximum(this.lineData.getXMax());
    this.topAxis.setXOffset(0)
    this.topAxis.setYOffset(0)
    this.topAxis.setDrawAxisLine(true)
    this.topAxis.setDrawLabels(true)
    this.topAxis.setDrawGridLines(false)
    this.topAxis.setTextColor(Color.White)

    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(this.lineData.getXMin());
    this.bottomAxis.setAxisMaximum(this.lineData.getXMax());
    this.bottomAxis.setDrawAxisLine(false);
    this.bottomAxis.setDrawLabels(false)
    this.bottomAxis.setDrawGridLines(false)

    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(6, false);
    this.leftAxis.setTextColor(ColorTemplate.getHoloBlue())
    this.leftAxis.setDrawGridLines(true);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setAxisMinimum(0);
    this.leftAxis.setAxisMaximum(200);
    this.leftAxis.setDrawAxisLine(true);
    this.leftAxis.setGranularityEnabled(true);

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(true);
    this.rightAxis.setLabelCount(6, false);
    this.rightAxis.setTextColor(Color.Red);
    this.rightAxis.setAxisMinimum(-200); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(900);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setDrawZeroLine(false);
    this.rightAxis.setGranularityEnabled(false);

    this.lineChartModel.setRootViewBgColor('#CCCCCC');
    this.lineChartModel.setTopAxis(this.topAxis);
    this.lineChartModel.setBottomAxis(this.bottomAxis);
    this.lineChartModel.setWidth(this.mWidth);
    this.lineChartModel.setHeight(this.mHeight);
    this.lineChartModel.setMinOffset(this.minOffset);
    if (this.leftAxis) {
      this.lineChartModel.setLeftAxis(this.leftAxis);
    }
    if (this.rightAxis) {
      this.lineChartModel.setRightAxis(this.rightAxis);
    }
    if (this.lineData) {
      this.lineChartModel.setLineData(this.lineData);
    }
    this.lineChartModel.setIsShowLegend(true);
    this.lineChartModel.setIsShowValue(false);
    this.lineChartModel.init();

  }

/**
   * 初始化数据
   * @param count  曲线图点的个数
   * @param range  y轴范围
   */
  private initCurveData(count: number, range: number): LineData {

    let values1 = new JArrayList<EntryOhos>();
    let values2 = new JArrayList<EntryOhos>();
    let values3 = new JArrayList<EntryOhos>();

    for (let i = 0; i < count; i++) {
      let yVal: number = Math.random() * (range / 2) + 50;
      values1.add(new EntryOhos(i, yVal));
    }

    for (let i = 0; i < count; i++) {
      let yVal: number = Math.random() * range + 450;
      values2.add(new EntryOhos(i, yVal));
    }

    for (let i = 0; i < count; i++) {
      let yVal: number = Math.random() * range + 500;
      values3.add(new EntryOhos(i, yVal));
    }

    let dataSet = new JArrayList<ILineDataSet>();

    let set1 = new LineDataSet(values1, "DataSet 1");
    set1.setDrawFilled(false);
    set1.setDrawValues(true);
    set1.setDrawCircles(true);
    set1.setDrawCircleHole(false)
    set1.setAxisDependency(AxisDependency.LEFT)
    set1.setCircleRadius(3);
    set1.setCircleColor(Color.White)
    set1.setColorByColor(ColorTemplate.colorRgb(140, 234, 255));
    set1.setLineWidth(2)
    dataSet.add(set1);

    let set2 = new LineDataSet(values2, "DataSet 2");
    set2.setDrawFilled(false);
    set2.setDrawValues(true);
    set2.setDrawCircles(true);
    set2.setDrawCircleHole(false)
    set2.setAxisDependency(AxisDependency.RIGHT)
    set2.setCircleRadius(3);
    set2.setCircleColor(Color.White)
    set2.setColorByColor(Color.Red);
    set2.setLineWidth(2)
    dataSet.add(set2);

    let set3 = new LineDataSet(values3, "DataSet 3");
    set3.setDrawFilled(false);
    set3.setDrawValues(true);
    set3.setDrawCircles(true);
    set3.setDrawCircleHole(false)
    set3.setAxisDependency(AxisDependency.RIGHT)
    set3.setCircleRadius(3);
    set3.setCircleColor(Color.White)
    set3.setColorByColor(Color.Yellow);
    set3.setLineWidth(2)
    dataSet.add(set3);

    return new LineData(dataSet)
  }

  build() {
    Column() {
      title({ model: this.model })
      Stack({ alignContent: Alignment.TopStart }) {
        LineChart({lineChartModel: $lineChartModel})
      }
    }
  }
}
