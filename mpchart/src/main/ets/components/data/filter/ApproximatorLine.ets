/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Implemented according to Wiki-Pseudocode
 */
export default class Approximator{
  private points: number[] = new Array<number>();
  private sxey: number;
  private exsy: number;
  private dx: number;
  private dy: number;
  private length: number;

  constructor(x1: number, y1: number, x2: number, y2: number) {
    this.dx = x1 - x2;
    this.dy = y1 - y2;
    this.sxey = x1 * y2;
    this.exsy = x2 * y1;
    this.length = Math.sqrt(this.dx * this.dx + this.dy * this.dy);
    this.points = [x1, y1, x2, y2];
  }

  public distance(x: number, y: number): number {
    return Math.abs(this.dy * x - this.dx * y + this.sxey - this.exsy) / this.length;
  }

  public getPoints(): number[]{
    return this.points;
  }
}

