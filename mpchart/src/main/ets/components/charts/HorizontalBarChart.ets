/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import XAxisView from '../components/renderer/XAxisView';
import YAxisView from '../components/renderer/YAxisView';
import DefaultValueFormatter from '../formatter/DefaultValueFormatter';
import { XAxis } from '../components/XAxis';
import LegendRenderer from '../renderer/LegendRenderer';
import Description from '../components/Description';
import Paint, { LinePaint,TextPaint,PathPaint,RectPaint , ImagePaint}  from '../data/Paint';
import ViewPortHandler from '../utils/ViewPortHandler';
import Transformer from '../utils/Transformer';
import YAxis from '../components/YAxis';
import MyRect from '../data/Rect';
import BarEntry from '../data/BarEntry';
import HorizontalBarChartRenderer from '../renderer/HorizontalBarChartRenderer';
import BarData from '../data/BarData'
import {AxisDependency} from '../components/YAxis'
import Legend from '../components/Legend'
import Utils from '../utils/Utils'
import IBarDataSet from '../interfaces/datasets/IBarDataSet'
import BarDataSet from '../data/BarDataSet';
import { JArrayList } from '../utils/JArrayList';
import ScaleMode from '../data/ScaleMode';
import EntryOhos from '../data/EntryOhos';

@Component
export default struct HorizontalBarChart {

  @ObjectLink model:HorizontalBarChartModel

  public aboutToAppear() {
    this.model.invalidate();
  }

  build(){
    Stack({alignContent:Alignment.TopStart}) {
      Stack() {
        XAxisView({ scaleMode:this.model });
      }
      .clip(new Path().commands(this.model.xAixsMode.clipPath))

      Stack(){
        YAxisView({ model: this.model.leftAxisModel})
        if(this.model.rightAxisModel.yAxis!=undefined){
          YAxisView({ model: this.model.rightAxisModel})
        }
      }

      Stack({alignContent:Alignment.TopStart}){
        Stack(){
          if(this.model.paints != null && this.model.paints.length > 0){
            ForEach(this.model.paints, (item: Paint) => {
              if (item instanceof LinePaint) {
                Line()
                  .width(this.model.mWidth)
                  .height(this.model.mHeight)
                  .startPoint(item.startPoint)
                  .endPoint(item.endPoint)
                  .fill(item.fill)
                  .stroke(item.stroke)
                  .strokeWidth(item.strokeWidth)
                  .strokeDashArray(item.strokeDashArray)
                  .strokeDashOffset(item.strokeDashOffset)
                  .strokeOpacity(item.alpha)
                  .position({ x: 0, y: 0 })
                  .visibility(item.visibility)
              } else if (item instanceof TextPaint) {
                Text(item.text)
                  .width(item.width)
                  .height(item.height)
                  .position({ x: item.x, y: item.y })
                  .fontWeight(item.typeface)
                  .fontSize(item.textSize)
                  .textAlign(item.textAlign)
                  .fontColor(item.color)
                  .visibility(item.visibility)
              } else if (item instanceof PathPaint) {
                Path()
                  .commands(item.commands)
                  .fill(item.fill)
                  .stroke(item.stroke)
                  .strokeWidth(item.strokeWidth == 0 ? 1 : item.strokeWidth)
                  .strokeDashArray(item.strokeDashArray)
                  .strokeDashOffset(item.strokeDashOffset)
                  .strokeOpacity(item.alpha)
                  .position({ x: item.x, y: item.y })
                  .visibility(item.visibility)
              }else if(item instanceof RectPaint){
                if(item.linearGradientColors != undefined && item.linearGradientColors.length > 0){
                  Text()
                    .width(item.width)
                    .height(item.height)
                    .borderColor(item.stroke)
                    .fontSize(item.textSize)
                    .fontColor(item.color)
                    .borderWidth(item.strokeWidth + "px")
                    .position({x:item.x,y:item.y})
                    .linearGradient({direction: GradientDirection.Top,
                      colors:item.linearGradientColors})
                    .visibility(item.visibility)
                }else{
                  Text()
                    .width(item.width)
                    .height(item.height)
                    .fontSize(item.textSize)
                    .fontColor(item.color)
                    .backgroundColor(item.fill)
                    .borderColor(item.stroke)
                    .borderWidth(item.strokeWidth +"px")
                    .position({x:item.x,y:item.y})
                    .visibility(item.visibility)
                }
              }
            }, (item: Paint) => JSON.stringify(item))
          }

          ForEach(this.model.clickPaints, (item: Paint) => {
            if(item instanceof ImagePaint){
              Image($r('app.media.marker2'))
                .width(item.width)
                .height(item.height)
                .objectFit(ImageFit.Fill)
                .position({x:item.x,y:item.y})
                .visibility(item.visibility)
            } else if (item instanceof TextPaint) {
              Text(item.text)
                .width(item.width)
                .height(item.height)
                .position({ x: item.x, y: item.y })
                .fontWeight(item.typeface)
                .fontSize(item.textSize)
                .textAlign(item.textAlign)
                .fontColor(item.color)
                .visibility(item.visibility)
            } else if(item instanceof RectPaint){
              Text()
                .width(item.width)
                .height(item.height)
                .position({x:item.x,y:item.y})
                .linearGradient({direction: GradientDirection.Top,
                  colors:item.linearGradientColors})
                .visibility(item.visibility)
            }
          }, (item: Paint) => JSON.stringify(item))
        }
        .width(this.model.mViewPortHandler.contentWidth())
        .height(this.model.mViewPortHandler.contentHeight())
        .position({x:this.model.translateX
        ,y:-this.model.finalViewPortHandler.contentTop()+this.model.translateY})
      }
      .width(this.model.finalViewPortHandler.contentWidth() - this.model.minOffset)
      .height(this.model.finalViewPortHandler.contentHeight())
      .position({y:this.model.finalViewPortHandler.contentTop()})
      .clip(new Path().commands(this.model.clipPath))
    }
    .onClick((event: ClickEvent |undefined)=>{
      if (event) {
        this.model.onClick(event)
      }
    })
    .onTouch((event: TouchEvent | undefined)=>{
      if (event) {
        this.model.onTouch(event)
      }
    })
  }

}
@Observed
export class HorizontalBarChartModel extends ScaleMode{
  /**
    * flag that indicates whether the highlight should be full-bar oriented, or single-value?
    */
  mHighlightFullBarEnabled:boolean = false;

  /**
    * if set to true, all values are drawn above their bars, instead of below their top
    */
  mDrawValueAboveBar:boolean = true;

  /**
     * if set to true, a grey area is drawn behind each bar that indicates the maximum value
     */
  mDrawBarShadow:boolean = false;

  mFitBars:boolean = false;
  mData:BarData = new BarData();
  mAxisLeft: YAxis  = new YAxis();
  mAxisRight: YAxis  = new YAxis();
  mAxisTop: XAxis = new XAxis();
  mAxisBottom: XAxis  = new XAxis();
  mLeftAxisTransformer: Transformer  | null = null;
  mRightAxisTransformer: Transformer  | null = null;
  mMaxVisibleCount: number = 100;
  mViewPortHandler: ViewPortHandler = new ViewPortHandler();
  mRenderer: HorizontalBarChartRenderer = new HorizontalBarChartRenderer(this, this.mViewPortHandler);
  finalViewPortHandler: ViewPortHandler = new ViewPortHandler();
  mWidth: number = 300;
  mHeight: number = 300;
  minOffset: number = 15;
  mDescription: Description = new Description();
  mLegendRenderer: LegendRenderer | null = null;
  mLegend: Legend | null = null;
  mDefaultValueFormatter: DefaultValueFormatter = new DefaultValueFormatter(0)
  paints:Paint[] = []
  private timerIdX: number = -1;
  private timerIdY: number = -1;
  private curvesX: number = 0;
  private curvesY: number = 0;
  clickPaints:Paint[] = [];
  mData2: BarData | null = null;
  translateX: number = 0;
  translateY: number = 0;
  translateCenterX: number = 0;
  translateCenterY: number = 0;
  clipPath: string = "";

  public init() {
    this.initViewPortHandler();
    this.mLeftAxisTransformer = new Transformer(this.mViewPortHandler);
    this.mRightAxisTransformer = new Transformer(this.mViewPortHandler);
    this.mRenderer = new HorizontalBarChartRenderer(this, this.mViewPortHandler);
    this.mDescription = new Description();
  }

  public initViewPortHandler(){
    this.mViewPortHandler.restrainViewPort(this.minOffset, this.minOffset, this.minOffset, this.minOffset)
    this.mViewPortHandler.setChartDimens(this.mWidth, this.mHeight);
    this.finalViewPortHandler.restrainViewPort(this.minOffset, this.minOffset, this.minOffset, this.minOffset)
    this.finalViewPortHandler.setChartDimens(this.mWidth,this.mHeight);
  }

  public resetViewPortHandler(){
    this.mViewPortHandler.restrainViewPort(this.minOffset, this.minOffset, this.minOffset, this.minOffset)
    this.mViewPortHandler.setChartDimens(this.mWidth, this.mHeight);
  }

  public prepareMatrixValuePx() {
    if (this.mRightAxisTransformer && this.mAxisBottom && this.mAxisLeft) {
      this.mRightAxisTransformer.prepareMatrixValuePx(this.mAxisBottom.mAxisMinimum,
        this.mAxisBottom.mAxisRange,
        this.mAxisLeft.mAxisRange,
        this.mAxisLeft.mAxisMinimum);
    }
    if (this.mLeftAxisTransformer && this.mAxisBottom && this.mAxisLeft) {
      this.mLeftAxisTransformer.prepareMatrixValuePx(this.mAxisBottom.mAxisMinimum,
        this.mAxisBottom.mAxisRange,
        this.mAxisLeft.mAxisRange,
        this.mAxisLeft.mAxisMinimum);
    }
  }

  protected calcMinMax() {

    //    if (this.mFitBars) {
    //      this.mXAxis.calculate(this.mData.getXMin() - this.mData.getBarWidth() / 2, this.mData.getXMax() + this.mData.getBarWidth() / 2);
    //    } else {
    //      this.mXAxis.calculate(this.mData.getXMin(), this.mData.getXMax());
    //    }
    //
    //    // calculate axis range (min / max) according to provided data
    //    this.mAxisLeft.calculate(this.mData.getYMin(AxisDependency.LEFT), this.mData.getYMax(AxisDependency.LEFT));
    //    this.mAxisRight.calculate(this.mData.getYMin(AxisDependency.RIGHT), this.mData.getYMax(AxisDependency
    //    .RIGHT));
  }

  /**
     * sets the number of maximum visible drawn values on the chart only active
     * when setDrawValues() is enabled
     *
     * @param count
     */
  public setMaxVisibleValueCount(count: number) {
    this.mMaxVisibleCount = count;
  }

  public getMaxVisibleCount(): number {
    return this.mMaxVisibleCount;
  }

  /**
     * Sets a new Description object for the chart.
     *
     * @param desc
     */
  public setDescription(desc: Description) {
    this.mDescription = desc;
  }

  /**
     * Returns the Description object of the chart that is responsible for holding all information related
     * to the description text that is displayed in the bottom right corner of the chart (by default).
     *
     * @return
     */
  public getDescription(): Description{
    return this.mDescription;
  }

  /**
    * Returns the y-axis object to the corresponding AxisDependency. In the
    * horizontal bar-chart, LEFT == top, RIGHT == BOTTOM
    *
    * @param axis
    * @return
    */
  public getAxis(axis:AxisDependency):YAxis | null{
    if (axis == AxisDependency.LEFT)
    return this.mAxisLeft;
    else
    return this.mAxisRight;
  }

  public isInverted(axis:AxisDependency):boolean {
    let axisResult = this.getAxis(axis);
    if (axisResult) {
      return axisResult.isInverted();
    }
    return false;
  }

  public setLeftYAxis(axis:YAxis){
    this.mAxisLeft = axis;
    if (this.mAxisLeft) {
      this.leftAxisModel.setYAxis(this.mAxisLeft);
    }
    this.leftAxisModel.setWidth(this.mWidth);
    this.leftAxisModel.setHeight(this.mHeight);
  }
  public setRightYAxis(axis:YAxis){
    this.mAxisRight = axis;
    if (this.mAxisRight) {
      this.rightAxisModel.setYAxis(this.mAxisRight);
    }
    this.rightAxisModel.setWidth(this.mWidth);
    this.rightAxisModel.setHeight(this.mHeight);
  }

//  public setRightYAxis(axis:YAxis){
//    this.mAxisRight = axis;
//  }

  public setBottomXAxis(axis:XAxis){
    this.mAxisBottom = axis;
    this.xAixsMode.bottomAxis=this.mAxisBottom
    this.xAixsMode.mWidth= this.mWidth
    this.xAixsMode.mHeight=this.mHeight
    this.xAixsMode.minOffset=this.minOffset
    if (this.mAxisLeft) {
      this.xAixsMode.yLeftLongestLabel=this.mAxisLeft.getAxisMaximum() + ""
      this.xAixsMode.yRightLongestLabel=this.mAxisLeft.getAxisMaximum() + ""
    }
    this.calcXAixsModeClipPath();
  }

  public calcModeClipPath(){
    let rect = this.finalViewPortHandler.getContentRect();
    let textPaint:TextPaint = new TextPaint();
    if (this.mAxisLeft) {
      textPaint.setTextSize(this.mAxisLeft.getTextSize())
      let maxTextOffset = Utils.calcTextWidth(textPaint,this.mAxisLeft.getAxisMinimum() < 0?this.mAxisLeft.getAxisMinimum().toFixed(0)+"":this.mAxisLeft.getAxisMaximum().toFixed(0)+"");;
      this.clipPath='M'+Utils.convertDpToPixel(rect.left+maxTextOffset)+' '+Utils.convertDpToPixel(0)
        +'L'+Utils.convertDpToPixel(rect.right+7)+' '+Utils.convertDpToPixel(0)
        +'L'+Utils.convertDpToPixel(rect.right+7)+' '+Utils.convertDpToPixel(this.finalViewPortHandler.contentBottom() - this.finalViewPortHandler.contentTop())
        +'L'+Utils.convertDpToPixel(rect.left+maxTextOffset)+' '+Utils.convertDpToPixel(this.finalViewPortHandler.contentBottom() - this.finalViewPortHandler.contentTop())
        +' Z'
    }
  }

  public calcXAixsModeClipPath(){
    let rect = this.finalViewPortHandler.getContentRect();
    let textPaint:TextPaint = new TextPaint();
    if (this.mAxisLeft) {
      textPaint.setTextSize(this.mAxisLeft.getTextSize())
      let maxTextOffset = Utils.calcTextWidth(textPaint,this.mAxisLeft.getAxisMinimum() < 0?this.mAxisLeft.getAxisMinimum().toFixed(0)+"":this.mAxisLeft.getAxisMaximum().toFixed(0)+"");;
      this.xAixsMode.clipPath='M'+Utils.convertDpToPixel(rect.left-5+maxTextOffset)+' '+Utils.convertDpToPixel(0)
        +'L'+Utils.convertDpToPixel(rect.right-10)+' '+Utils.convertDpToPixel(0)
        +'L'+Utils.convertDpToPixel(rect.right-10)+' '+Utils.convertDpToPixel(this.mHeight)
        +'L'+Utils.convertDpToPixel(rect.left-5+maxTextOffset)+' '+Utils.convertDpToPixel(this.mHeight)
        +' Z'
    }
  }

  public getBottomXAxis():XAxis | null{
    return this.mAxisBottom;
  }

  public setTopXAxis(axis:XAxis){
    this.mAxisTop = axis;
    this.xAixsMode.topAxis = this.mAxisTop
  }

  public getTopXAxis():XAxis | null{
    return this.mAxisTop;
  }

  public getTransformer(which:AxisDependency):Transformer | null {
    if (which == AxisDependency.LEFT){
      return this.mLeftAxisTransformer;
    }else{
      return this.mRightAxisTransformer;
    }
  }

  public isDrawingValuesAllowed(): boolean{
    if (this.mData) {
      return this.mData.getEntryCount() < this.mMaxVisibleCount * this.mViewPortHandler.getScaleX();
    }
    return true;
  }

  /**
     * Returns the Highlight object (contains x-index and DataSet index) of the selected value at the given touch
     * point
     * inside the BarChart.
     *
     * @param x
     * @param y
     * @return
     */
  public getHighlightByTouchPoint(x:number, y:number)/*:Highlight*/ {

    //    if (this.mData == null) {
    //      console.error("Can't select by touch. No data set.")
    //      return null;
    //    } else {
    //      let h:Highlight = this.getHighlighter().getHighlight(x, y);
    //      if (h == null || !this.isHighlightFullBarEnabled()) return h;
    //
    //      // For isHighlightFullBarEnabled, remove stackIndex
    //      return new Highlight(h.getX(), h.getY(),
    //      h.getXPx(), h.getYPx(),
    //      h.getDataSetIndex(), -1, h.getAxis());
    //    }
  }

  /**
     * Returns the bounding box of the specified Entry in the specified DataSet. Returns null if the Entry could not be
     * found in the charts data.  Performance-intensive code should use void getBarBounds(BarEntry, RectF) instead.
     *
     * @param e
     * @return
     */
  public getBarBounds(e:BarEntry):MyRect {

    let bounds:MyRect = new MyRect();
    this.getBarBoundsRect(e, bounds);

    return bounds;
  }

  /**
     * The passed outputRect will be assigned the values of the bounding box of the specified Entry in the specified DataSet.
     * The rect will be assigned Float.MIN_VALUE in all locations if the Entry could not be found in the charts data.
     *
     * @param e
     * @return
     */
  public getBarBoundsRect( e:BarEntry,outputRect:MyRect) {

    //    let bounds:MyRect = outputRect;
    //
    //    let dataSet:IBarDataSet = this.mData.getDataSetForEntry(e);
    //
    //    if (dataSet == null) {
    //      this.bounds.set(Number.MIN_VALUE, Number.MIN_VALUE, Number.MIN_VALUE, Number.MIN_VALUE);
    //      return;
    //    }
    //
    //    let y:number = e.getY();
    //    let x:number = e.getX();
    //
    //    let barWidth:number = this.mData.getBarWidth();
    //
    //    let left:number = x - barWidth / 2;
    //    let right:number = x + barWidth / 2;
    //    let top:number = y >= 0 ? y : 0;
    //    let bottom:number = y <= 0 ? y : 0;
    //
    //    bounds.set(left, top, right, bottom);
    //
    //    this.getTransformer(dataSet.getAxisDependency()).rectValueToPixel(outputRect);
  }

  /**
     * If set to true, all values are drawn above their bars, instead of below their top.
     *
     * @param enabled
     */
  public setDrawValueAboveBar(enabled:boolean) {
    this.mDrawValueAboveBar = enabled;
  }

  /**
     * returns true if drawing values above bars is enabled, false if not
     *
     * @return
     */
  public isDrawValueAboveBarEnabled():boolean {
    return this.mDrawValueAboveBar;
  }

  /**
     * If set to true, a grey area is drawn behind each bar that indicates the maximum value. Enabling his will reduce
     * performance by about 50%.
     *
     * @param enabled
     */
  public setDrawBarShadow( enabled:boolean) {
    this.mDrawBarShadow = enabled;
  }

  /**
     * returns true if drawing shadows (maxvalue) for each bar is enabled, false if not
     *
     * @return
     */
  public isDrawBarShadowEnabled():boolean {
    return this.mDrawBarShadow;
  }

  /**
     * Set this to true to make the highlight operation full-bar oriented, false to make it highlight single values (relevant
     * only for stacked). If enabled, highlighting operations will highlight the whole bar, even if only a single stack entry
     * was tapped.
     * Default: false
     *
     * @param enabled
     */
  public setHighlightFullBarEnabled( enabled:boolean) {
    this.mHighlightFullBarEnabled = enabled;
  }

  /**
     * @return true the highlight operation is be full-bar oriented, false if single-value
     */
  public isHighlightFullBarEnabled():boolean {
    return this.mHighlightFullBarEnabled;
  }

  /**
     * Highlights the value at the given x-value in the given DataSet. Provide
     * -1 as the dataSetIndex to undo all highlighting.
     *
     * @param x
     * @param dataSetIndex
     * @param stackIndex   the index inside the stack - only relevant for stacked entries
     */
  public highlightValue( x:number, dataSetIndex:number,stackIndex:number) {
    //    this.highlightValue(new Highlight(x, dataSetIndex, stackIndex), false);
  }

  public getBarData():BarData{
    return this.mData;
  }

  public setBarData(data:BarData){
    this.mData = data;
  }

  /**
     * Adds half of the bar width to each side of the x-axis range in order to allow the bars of the barchart to be
     * fully displayed.
     * Default: false
     *
     * @param enabled
     */
  public setFitBars( enabled:boolean) {
    this.mFitBars = enabled;
  }

  /**
     * Groups all BarDataSet objects this data object holds together by modifying the x-value of their entries.
     * Previously set x-values of entries will be overwritten. Leaves space between bars and groups as specified
     * by the parameters.
     * Calls notifyDataSetChanged() afterwards.
     *
     * @param fromX      the starting point on the x-axis where the grouping should begin
     * @param groupSpace the space between groups of bars in values (not pixels) e.g. 0.8f for bar width 1f
     * @param barSpace   the space between individual bars in values (not pixels) e.g. 0.1f for bar width 1f
     */
  public groupBars(fromX:number, groupSpace:number,barSpace:number) {
    let barData = this.getBarData();

    if (barData == null) {
      throw new Error("You need to set data for the chart before grouping bars.");
    } else {
      barData.groupBars(fromX, groupSpace, barSpace);
      this.notifyDataSetChanged();
    }
  }

  public notifyDataSetChanged() {

    if (this.mRenderer != null)
    this.mRenderer.initBuffers();

    this.calcMinMax();

    //    this.mAxisRendererLeft.computeAxis(this.mAxisLeft.mAxisMinimum, this.mAxisLeft.mAxisMaximum, this.mAxisLeft.isInverted());
    //    this.mAxisRendererRight.computeAxis(this.mAxisRight.mAxisMinimum, this.mAxisRight.mAxisMaximum, this.mAxisRight.isInverted());
    //    this.mXAxisRenderer.computeAxis(this.mXAxis.mAxisMinimum, this.mXAxis.mAxisMaximum, false);

    if (this.mLegendRenderer && this.mData)
    this.mLegendRenderer.computeLegend(this.mData);

    //    this.calculateOffsets();
  }

  public invalidate(){
    this.paints = [];
    if (this.mRenderer) {
      this.paints = this.paints.concat(this.mRenderer.drawData());
      this.paints = this.paints.concat(this.mRenderer.drawValues());
    }
    this.calcModeClipPath();
  }

  public setData(data: BarData) {

    this.mData = data;
    let dataSets2: JArrayList<IBarDataSet> = new JArrayList();
    let dataSets = data.getDataSets();
    for(let i = 0;i < dataSets.length();i++){
      let dataSetByIndex = data.getDataSetByIndex(i);
      if(dataSetByIndex){
        let entries = dataSetByIndex.getEntries();
        if (entries) {
          dataSets2.add(new BarDataSet(entries as JArrayList<BarEntry>,dataSetByIndex.getLabel()));
        }
      }
    }
    this.mData2 = new BarData(dataSets2);
    //        this.mOffsetsCalculated = false;

    if (data == null) {
      return;
    }

    // calculate how many digits are needed
    this.setupDefaultFormatter(data.getYMin(), data.getYMax());

    for(let i = 0;i < this.mData.getDataSets().length();i++){
      let dataSet = this.mData.getDataSets().get(i);
      if (dataSet) {
        let valueFormatter = dataSet.getValueFormatter();
        if (valueFormatter) {
          if (dataSet.needsFormatter() || valueFormatter == this.mDefaultValueFormatter)
            dataSet.setValueFormatter(this.mDefaultValueFormatter);
        }
      }
    }

    // let the chart know there is new data
    this.notifyDataSetChanged();

  }

  /**
     * Calculates the required number of digits for the values that might be
     * drawn in the chart (if enabled), and creates the default-value-formatter
     */
  protected setupDefaultFormatter(min: number,max: number) {

    let reference: number = 0;

    if (this.mData == null || this.mData.getEntryCount() < 2) {

      reference = Math.max(Math.abs(min), Math.abs(max));
    } else {
      reference = Math.abs(max - min);
    }

    let digits: number = Utils.getDecimals(reference);

    // setup the formatter with a new number of digits
    this.mDefaultValueFormatter.setup(digits);
  }

  public animateX(durationMillis: number) {
    if (this.curvesY == 1) {
      this.curvesY = 0
      clearInterval(this.timerIdY)
    }
    clearInterval(this.timerIdX)
    this.curvesX = 0;
    this.timerIdX = setInterval(()=>{
      let barData =  this.getBarData();
      if (!barData){
        return;
      }
      this.curvesX += 10;
      let count = this.paints.length;
      let dataSetByIndex = barData.getDataSetByIndex(0);
      if (dataSetByIndex) {
        let isDrawValues = this.isDrawingValuesAllowed() && dataSetByIndex.isDrawValuesEnabled();
        if(isDrawValues){
          count = count / 2;
        }
        let position = Math.floor(this.curvesX / (durationMillis / count))
        for(let i = 0;i < count;i++){
          this.paints[i].setVisibility(position >= i?Visibility.Visible:Visibility.Hidden)
          if(isDrawValues){
            this.paints[i + this.paints.length / 2].setVisibility(position >= i?Visibility.Visible:Visibility.Hidden)
          }
        }
        if(this.clickPaints.length >= 1){
          this.clickPaints[0].visibility = position >= this.clickPaints[0].clickPosition?Visibility.Visible:Visibility.Hidden;
        }
        if(this.clickPaints.length == 3){
          this.clickPaints[1].visibility = position >= this.clickPaints[0].clickPosition?Visibility.Visible:Visibility.Hidden;
          this.clickPaints[2].visibility = position >= this.clickPaints[0].clickPosition?Visibility.Visible:Visibility.Hidden;
        }
        if(this.curvesX >= durationMillis){
          this.curvesX = 0;
          clearInterval(this.timerIdX)
        }
      }
    }, 40)
  }

  public animateY(durationMillis: number) {
    if (!this.mData2) {
      return;
    }
    clearInterval(this.timerIdY)
    clearInterval(this.timerIdX)
    this.curvesY = 0;
    this.curvesX = 0;
    // let clickPaints2 = JSON.parse(JSON.stringify(this.clickPaints))
    this.timerIdY = setInterval(()=>{
      if (!this.mData2) {
        return;
      }
      let barData = this.getBarData();
      this.curvesY += 0.01 / (durationMillis / 1000);
      if (!barData) {
        return;
      }

      for(let i = 0;i < barData.getDataSetCount();i++){
        let dataSetByIndex = this.mData2!.getDataSetByIndex(i);
        if (dataSetByIndex) {
          let entries = dataSetByIndex.getEntries();
        if (entries) {
          let values: JArrayList<BarEntry> = new JArrayList();
          for(let j = 0;j < entries.length();j++){
            let barEntry: BarEntry = entries.get(j) as BarEntry;
            let barEntryYValues = barEntry.getYVals();
            if(barEntry.isStacked() && barEntryYValues){
              let yVals: number[] = [];
              for(let j = 0;j < barEntryYValues.length;j++){
                yVals[j] = barEntryYValues[j] * this.curvesY;
              }
              values.add(new BarEntry(barEntry.getX(),yVals))
            }else{
              values.add(new BarEntry(barEntry.getX() * this.curvesY,barEntry.getY()))
            }
          }
          (dataSetByIndex as BarDataSet).setValues(values)
        }
        }
      }
      barData.notifyDataChanged();
      this.notifyDataSetChanged();
      this.invalidate();

      if(this.curvesY >= 1){
        this.curvesY = 0;
        clearInterval(this.timerIdY)
      }
    },40)
  }

  public animateXY(durationMillisX: number,durationMillisY: number){
    this.animateY(durationMillisY);
    this.animateX(durationMillisX);
  }

  public onSingleClick(event: ClickEvent){
    let x = event.x;
    let y = event.y;
    let position = -1;
    for(let i = 0;i < this.paints.length;i++){
      if(this.paints[i] instanceof RectPaint){
        let rectPaint:RectPaint = this.paints[i] as RectPaint;
        if(x >= rectPaint.x + this.translateX && x <= (rectPaint.x + Number(rectPaint.width)+this.translateX)
        && y >= rectPaint.y + this.translateY && y <= (rectPaint.y + Number(rectPaint.height)+this.translateY)){
          position = i;
          this.click(position)
          break;
        }
      }
    }
  }

  public onDoubleClick(event: ClickEvent){
    this.scale()
    if(this.clickPaints.length > 0){
      this.click(this.clickPaints[0].clickPosition)
    }
    this.translateCenterX = this.checkTranslateX((this.centerX * this.scaleX - this.centerX));
    this.translateCenterY = this.checkTranslateY(-(this.centerY * this.scaleY - this.centerY))
    this.translateX = this.translateCenterX;
    this.setXPosition(this.translateX)
  }

  public onMove(event: TouchEvent){
    let translateX = this.translateCenterX + this.moveX;
    let translateY = this.translateCenterY + this.moveY;

    this.translateX = this.checkTranslateX(translateX);
    this.moveX = this.translateX - this.translateCenterX
    this.translateY = this.checkTranslateY(translateY);
    this.moveY = this.translateY - this.translateCenterY
    this.leftAxisModel.translate(this.translateY);
    this.rightAxisModel.translate(this.translateY);
    this.setXPosition(this.translateX)
  }

  public onScale(event: TouchEvent){
    this.scale()
    if(this.clickPaints.length > 0){
      this.click(this.clickPaints[0].clickPosition)
    }
    this.translateCenterX = this.checkTranslateX(-(this.centerX * this.scaleX - this.centerX));
    this.translateCenterY = this.checkTranslateY(-(this.centerY * this.scaleY - this.centerY))
    this.translateX = this.translateCenterX;
    this.setXPosition(this.translateX)
  }

  private scale(){
    this.mWidth = this.finalViewPortHandler.getChartWidth() * this.scaleX;
    this.mHeight = this.finalViewPortHandler.getChartHeight() * this.scaleY;
    this.xAixsMode.mWidth = this.mWidth;
    this.resetViewPortHandler();
    this.invalidate();
  }

  private click(position:number){
    let textPaint:TextPaint| null = null;
    let paints2 = this.paints.concat([]);
    let rectPaint = this.paints[position];
    let barData = this.getBarData();
    if (barData){
      if(barData.getDataSets().get(0).isDrawValuesEnabled()){
        paints2.splice(0,paints2.length / 2);
        textPaint = paints2[position] as TextPaint;
        if (this.mRenderer) {
          this.clickPaints = this.mRenderer.drawClicked(rectPaint,textPaint,paints2.length,position);
        }
      }
    }
  }

  private getMaxWidth(): number{
    return this.mViewPortHandler.getChartWidth();
  }

  private checkTranslateX(translateX: number): number{
    if(translateX > 0){
      translateX = 0;
    }
    let maxWidth = this.getMaxWidth() - this.finalViewPortHandler.getChartWidth();
    if(translateX < -maxWidth){
      translateX = -maxWidth;
    }
    return translateX;
  }

  private checkTranslateY(translateY: number): number{
    if(translateY < 0){
      translateY = 0;
    }
    let maxHeight = this.leftAxisModel.mHeight - this.finalViewPortHandler.getChartHeight()
    if(translateY > maxHeight){
      translateY = maxHeight;
    }
    return translateY;
  }

}
